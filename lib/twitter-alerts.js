var EventEmitter = require('events').EventEmitter;
var moment = require('moment');
var util = require('util');

/**
 * Constructor
 */
function TwitterAlerts() {
	if (!(this instanceof TwitterAlerts)) return new TwitterAlerts();
	EventEmitter.call(this);

	this.history = [];
	this.historyLength = 50;
}

util.inherits(TwitterAlerts, EventEmitter);

/**
 * Parse a JSON array related to alert
 *
 * @param Array JSON array to parse
 * @return Array Alert data
 */
TwitterAlerts.parse = function(tweet) {
	var pattern = /(.*)\s+\((.*)\): (.*) - (\d+)m - (\d+)cr(?: - (.*)\s+\((.*)\))?/;
	if (!pattern.test(tweet.text))
		return null;
	var matches = pattern.exec(tweet.text);
	var beginDate = moment.utc(TwitterAlerts.parseDate(tweet.created_at));
	var endDate = moment(beginDate);
	endDate.add('minutes', matches[4]);
	var alert = {
		'location': matches[1],
		'planet': matches[2],
		'mission': matches[3],
		'begin': beginDate,
		'end': endDate,
		'credits': matches[5]
	};
	if (matches[6]) {
		alert['reward'] = {
			'name': matches[6],
			'type': matches[7]
		};
	}
	return alert;
}

TwitterAlerts.prototype.add = function(tweet) {
	var alert = TwitterAlerts.parse(tweet);
	if (alert) {
		this.history.unshift(alert);
		this.emit('add', alert);
		this.history.slice(0, this.historyLength);
	}
	return alert;
}

TwitterAlerts.prototype.addMany = function(tweets) {
	var self = this;
	tweets.forEach(function(tweet) {
		self.add(tweet);
	});
	this.sort();
}

TwitterAlerts.prototype.sort = function() {
	this.history = TwitterAlerts.sort(this.history);
}

TwitterAlerts.sort = function(alerts) {
	return alerts.sort(function(a, b) {
		if (a.begin < b.begin)
			return 1;
		if (a.begin > b.begin)
			return -1;
		return 0;
	});
}

TwitterAlerts.parseDate = function(date) {
	return Date.parse(date.replace(/( \+)/, ' UTC$1'));
}

module.exports = new TwitterAlerts;
