/**
 * Load dependencies
 */
var config = require('./config');
var express = require('express');
var http = require('http');
var twitter = require('ntwitter');
var twitterAlerts = require('./lib/twitter-alerts');
var initialized = false;

/**
 * Configure alert history length
 */
twitterAlerts.historyLength = config.history.count;

/**
 * Enable alert notification logging
 */
twitterAlerts.on('add', function(alert) {
	console.info('New alert : %s', alert.mission);
});

/**
 * Initialize the Twitter API
 */
var twit = new twitter(config.twitter.credentials);

/**
 * Get the latest alerts
 */
twit.getUserTimeline({user_id: config.twitter.user, count: config.history.count}, function(err, tweets) {
	if (err) {
		console.error(err);
	} else {
		twitterAlerts.addMany(tweets.reverse());
		initialized = true;
	}
});

/**
 * Start the stream of next incoming alerts
 */
twit.stream('statuses/filter', {follow: config.twitter.user}, function(stream) {
	stream.on('data', function(data) {
		if (data.user) {
			if (data.user.id == config.twitter.user)
				twitterAlerts.add(data);
		} else {
			console.log('Unknown data incoming : %s', JSON.stringify(data));
		}
	}).on('error', function(type, statusCode) {
		console.error('An error statusCode as returned during streaming : %d', statusCode);
	});
});

/**
 * Enable REST API server if specified
 */
if (config.server.rest_api) {

	/**
	 * Initialize the web server
	 */
	var app = express();
	var server = http.createServer(app);

	app.enable('trust proxy');

	/**
	 * Start server listening
	 */
	server.listen(config.server.rest_api.port, config.server.rest_api.hostname, function() {
		var addr = this.address();
		console.info('REST API server listening at http://%s:%d/', addr.address, addr.port);
	});

	/**
	 * Configure routes
	 */
	app.get('/alerts', function(request, response) {
		var alerts = twitterAlerts.history;
		var query = request.query;
		if (query) {
			alerts = alerts.filter(function(value) {
				if (query.reward_name) {
					return value.reward && value.reward.name && (value.reward.name.indexOf(query.reward_name) != -1);
				}
				if (query.reward_type) {
					return value.reward && value.reward.type && (value.reward.type.indexOf(query.reward_type) != -1);
				}
				if (query.has_reward) {
					return (query.has_reward == 'true' ? value.reward : !value.reward);
				}
				return true;
			});
			alerts = alerts.slice(0, query.count || config.defaults.count);
		}
		response.writeHead(200, {'Content-Type': 'application/json'});
		response.end(JSON.stringify(alerts));
	});

}

/**
 * Enable stream server if specified
 */
if (config.server.stream) {

	/**
	 * Initialize the stream server
	 */
	var app = express();
	var server = http.createServer(app);

	app.enable('trust proxy');

	/**
	 * Start server listening
	 */
	server.listen(config.server.stream.port, config.server.stream.hostname, function() {
		var addr = this.address();
		console.info('Stream server listening at http://%s:%d/', addr.address, addr.port);
	});

	/**
	 * Initialize the socket system
	 */
	var io = require('socket.io').listen(server, {log: false});

	io.configure('development', function() {
		io.set('transports', ['xhr-polling']);
	});

	/**
	 * Configure routes
	 */
	var sockets = io.of('/alerts').on('connection', function(socket) {
		var alerts = twitterAlerts.history.slice(0, 5);
		alerts.reverse().forEach(function(alert) {
			socket.emit('message', alert);
		});
	});

	/**
	 * Emit alert notifications sockets
	 */
	twitterAlerts.on('add', function(alert) {
		if (initialized)
			sockets.emit('message', alert);
	});

}
