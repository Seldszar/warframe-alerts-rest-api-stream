# Node.js Warframe Alerts #

## Setup ##

### Build dependencies ###

Type `npm install` in the installation directory to install required dependencies.

## Branch strategy ##

This repository is splitted into following branches :

 * **master** : releases and stable revisions
 * **develop** : new features
